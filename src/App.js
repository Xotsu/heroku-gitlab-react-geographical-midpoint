import React, { Component } from 'react';
import { Map, TileLayer, Marker } from 'react-leaflet';
import * as turf from "@turf/turf";
import blueIcon from './img/blueIcon.png'
import greenIcon from './img/greenIcon.png'
import redIcon from './img/redIcon.png'
import L from 'leaflet'
import './App.css';

var greenIconLogo = L.icon({
  iconUrl: greenIcon,
  iconSize: [32, 50],
  iconAnchor: [16, 50],
  popupAnchor: [-6, -76],
});
var blueIconLogo = L.icon({
  iconUrl: blueIcon,
  iconSize: [50, 50],
  iconAnchor: [25, 50],
  popupAnchor: [3, -76],
});
var redIconLogo = L.icon({
  iconUrl: redIcon,
  iconSize: [40, 50],
  iconAnchor: [20, 50],
  popupAnchor: [3, -76],
});



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      start: {
        lat: 0.515,
        lng: -0.09
      },
      middle: {
        lat: 0,
        lng: 0
      },
      zoom: 2,
      marker1: {
        lat: 51.485,
        lng: -0.09,
      },
      marker2: {
        lat: 51.540,
        lng: -0.09
      },
      showMarkers: false,
      showRedMarker: true,
      distance: 0
    }
    this.myRef1 = React.createRef();
    this.myRef2 = React.createRef();
  }


  middlePoint = (lat1, lng1, lat2, lng2) => {
    const point1 = turf.point([lat1, lng1]);
    const point2 = turf.point([lat2, lng2]);
    const result = turf.midpoint(point1, point2);

    return result.geometry.coordinates

  }

  distance = (lat1, lon1, lat2, lon2) => {
    const R = 6371e3; // metres
    const φ1 = lat1 * Math.PI / 180; // φ, λ in radians
    const φ2 = lat2 * Math.PI / 180;
    const Δφ = (lat2 - lat1) * Math.PI / 180;
    const Δλ = (lon2 - lon1) * Math.PI / 180;

    const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) *
      Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c;
    return d
  }
  dispalyDistance = (x) => {
    return x < 1000 ? ("Distance between markers: " + Math.floor(this.state.distance) + "m") : "Distance between markers: " + (this.state.distance / 1000).toFixed(2) + "km"
  }

  updatePosition1 = () => {
    const marker1 = this.myRef1.current
    if (marker1 != null) {
      this.setState({
        marker1: marker1.leafletElement.getLatLng(),
      })
      this.updateMid()
    }
  }
  updatePosition2 = () => {
    const marker2 = this.myRef2.current
    if (marker2 != null) {
      this.setState({
        marker2: marker2.leafletElement.getLatLng(),
      })
    }
    this.updateMid()
  }
  updateMid = () => {
    let arr = this.middlePoint(this.state.marker1.lat, this.state.marker1.lng, this.state.marker2.lat, this.state.marker2.lng)
    console.log(1111, arr)
    let d = this.distance(this.state.marker1.lat, this.state.marker1.lng, this.state.marker2.lat, this.state.marker2.lng)
    this.setState({
      middle: {
        lat: arr[0],
        lng: arr[1]
      },
      distance: d
    })
  }
  componentDidMount() {
    let arr = this.middlePoint(this.state.marker1.lat, this.state.marker1.lng, this.state.marker2.lat, this.state.marker2.lng)
    console.log(2222, arr)

    this.setState({
      middle: {
        lat: arr[0],
        lng: arr[1]
      }
    })
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({
        start: {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        },
        zoom: 13,
        marker1: {
          lat: position.coords.latitude + 0.02,
          lng: position.coords.longitude - 0.01
        },
        marker2: {
          lat: position.coords.latitude + 0.02,
          lng: position.coords.longitude + 0.01
        },
        showMarkers: true
      })
      this.updateMid()
    }, () => {
      fetch("https://ipapi.co/json")
        .then(res => res.json())
        .then(location => {
          this.setState({
            start: {
              lat: location.latitude,
              lng: location.longitude
            },
            zoom: 13,
            marker1: {
              lat: location.latitude + 0.02,
              lng: location.longitude - 0.01
            },
            marker2: {
              lat: location.latitude + 0.02,
              lng: location.longitude + 0.01
            },
            showMarkers: true
          })
          this.updateMid()
        })
    })
  }


  render() {
    let position = [this.state.start.lat, this.state.start.lng]
    let positionMid = [this.state.middle.lat, this.state.middle.lng]
    let marker1Position = [this.state.marker1.lat, this.state.marker1.lng]
    let marker2Position = [this.state.marker2.lat, this.state.marker2.lng]

    return (
      <div className="map">
        {/* <textarea name="hello" id="" cols="30" rows="10" placeholder="Test"></textarea> */}

        <Map className="map" ref="map" center={position} zoom={this.state.zoom}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          {this.state.showMarkers && <div>
            <Marker position={marker1Position}
              draggable="true"
              onDragend={this.updatePosition1}
              ref={this.myRef1}
              icon={greenIconLogo}>
            </Marker>
            <Marker position={marker2Position}
              draggable="true"
              onDragend={this.updatePosition2}
              ref={this.myRef2}
              icon={blueIconLogo}>
            </Marker>
            {this.state.showRedMarker &&
              <Marker position={positionMid}
                icon={redIconLogo}>
              </Marker>}

          </div>}
        </Map>
        <button id="showMarkers" onClick={() => {
          this.setState({
            showMarkers: !this.state.showMarkers
          });
          if (!this.state.showMarkers) {
            let bounds = this.refs.map.leafletElement.getBounds();
            this.setState({
              marker1: {
                lat: (bounds._northEast.lat + bounds._southWest.lat) / 2,
                lng: (bounds._northEast.lng + bounds._southWest.lng) / 2 + (bounds._northEast.lng - bounds._southWest.lng) / 10
              },
              marker2: {
                lat: (bounds._northEast.lat + bounds._southWest.lat) / 2,
                lng: (bounds._northEast.lng + bounds._southWest.lng) / 2 - (bounds._northEast.lng - bounds._southWest.lng) / 10
              }
            }, () => {
              this.updateMid()
            })
          }
        }
        }>{this.state.showMarkers ? "Hide Markers!" : "Show Markers!"}</button>
        <button id="positionMarkers" onClick={() => {
          let bounds = this.refs.map.leafletElement.getBounds();
          this.setState({
            marker1: {
              lat: (bounds._northEast.lat + bounds._southWest.lat) / 2,
              lng: (bounds._northEast.lng + bounds._southWest.lng) / 2 + (bounds._northEast.lng - bounds._southWest.lng) / 10
            },
            marker2: {
              lat: (bounds._northEast.lat + bounds._southWest.lat) / 2,
              lng: (bounds._northEast.lng + bounds._southWest.lng) / 2 - (bounds._northEast.lng - bounds._southWest.lng) / 10
            }
          }, () => {
            this.updateMid()
          })
        }
        }>Reposition Markers!</button>
        <button id="showRedMarker" onClick={() => {
          this.setState({
            showRedMarker: !this.state.showRedMarker
          })
        }}>Show Mid Point!</button>
        <textarea disabled id="distance" value={this.dispalyDistance(this.state.distance)}></textarea>
      </div >
    )
  }
}

export default App;
